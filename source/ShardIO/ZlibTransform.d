/// Provides DataTransforms for compression and decompression using the zlib library.
module ShardIO.ZlibTransform;

// TODO: Requires a fair bit of effort to make this work efficiently.
// We don't know the uncompressed size for compressed data, so we could either
// guess and allocate too large a buffer to be safe, or we could improve IOAction
// so that it supports multiple buffers returned from transforms.
// This gets complicated though.

version(none) {
import etc.c.zlib;

public import ShardIO.DataTransform;
import std.exception;

/// Provides an InputTransform that decompresses using the inflate algorithm.
class InflateTransform : InputTransform {
	/// Creates a new InflateTransform with the given order.
	/// No compression level is required, as zlib will automatically handle that.
	this(int order) {
		super(order);
		inflateInit(&zs);
	}

	~this() {
		inflateEnd(&zs);
	}

	override ubyte[] transform(ubyte[] buffer) {
		zs.avail_in = buffer.length;
	}

private:
	z_stream zs;
}

/// Provides an OutputTransform that compresses using the deflate algorithm.
class DeflateTransform : OutputTransform {
	/// Creates a new DeflateTransform with the given order and compression level.
	/// The compression level is between 1 and 9, with the default value being -1, which zlib treats as 6.
	this(int order, int level = Z_DEFAULT_COMPRESSION) {
		enforce(level == -1 || (level >= 1 && level <= 9));
		deflateInit(&zs, level);
	}

	~this() {
		deflateEnd(&zs);
	}

private:
	z_stream zs;
}
}