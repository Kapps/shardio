﻿module ShardIO.DataSource;
private import core.sync.mutex;
private import std.exception;
import ShardTools.Untyped;
public import ShardIO.IOAction;
//import ShardIO.DataTransformerCollection;
import ShardTools.ExceptionTools;
import core.atomic;

/// Indicates whether to do a read operation, write operation, or both a read and a write.
enum DataOperation {
	None = 0,
	Read = 1,
	Write = 2,
	Both = Read | Write
}

/// Provides flags for when data is requested from a DataSource.
enum DataRequestFlags {
	/// No flags are set.
	None = 0,
	/// The operation was successful, but more data may be available. If Waiting is not set, it may be queried immediately.
	Continue = 1,
	/// There was no data available. Will wait for more and then notify.
	Waiting = 2,
	/// This call marks the completion of this source's data. For an InputSource, this means the data should be written to then the Action complete. For an OutputSource, this means the action is complete.
	Complete = 4,
	/// Additional data is required to complete this call. This flag is only valid for an OutputSource.
	NeedMore = 8
}

/// Provides the base class for either an InputSource or OutputSource.
/// DataSources are similar to one-way streams, with all methods being asynchronous.
/// DataSources can read or write a variable amount of data, ideally close to a certain limit.
/// Any data that can not be operated on immediately is buffered.
/// Once the buffers are getting close to being emptied, the DataSource requests additional data
/// from the other source.
abstract class DataSource  {

public:
	/// Initializes a new instance of the DataSource object.
	this() {

	}

	/// Gets the action this DataSource is a part of.
	/// If this DataSource is not yet part of an action (because no IOAction has been created that uses it), this returns null.
	final @property IOAction Action() {		
		return _Action;
	}

	/// Closes this DataSource, provided that it is not in use by an IOAction.
	/// This method is valid only if no IOAction has been started using this DataSource,
	/// and is generally used for cleanup when creating the other DataSource may fail.
	/// The DataSource will be automatically closed once an IOAction with it completes.
	/// Attempting to close an already closed DataSource will result in no operation.
	final void Close() {
		synchronized(this) {
			if(_IsClosed)
				return;
			if(Action && Action.HasBegun && Action.Status == CompletionType.Incomplete)
				throw new NotSupportedException("Unable to close a DataSource when it is in use.");
			_IsClosed = true;
		}
		PerformClose();
	}

package:

	void NotifyInitialize(IOAction Action) {
		Initialize(Action);
	}

protected:

	/// Called to initialize the DataSource after the action is set.
	/// Any DataSources that require access to the IOAction they are part of should use this to do so.
	void Initialize(IOAction Action) {	
		synchronized(this) {		
			this._Action = Action;
			Action.NotifyOnComplete(Untyped.init, &OnCompleteInternal);
		}
	}

	private void OnCompleteInternal(Untyped State, AsyncAction Action, CompletionType Type) {
		OnComplete(cast(IOAction)Action, Type);
		PerformClose();
	}

	/// Occurs when the action completes for whatever reason.
	protected void OnComplete(IOAction Action, CompletionType Type) {

	}

	/// Implement to handle closing of this DataSource.
	/// This is automatically called when the IOAction completes (whether successfully or due to a failure).
	protected abstract void PerformClose();
	
private:		
	IOAction _Action;
	bool _IsClosed;
}