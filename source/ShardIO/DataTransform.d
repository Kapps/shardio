module ShardIO.DataTransform;

import ShardIO.DataSource;
import core.sync.mutex;
import std.algorithm;
import std.array;
import std.range;
import ShardTools.ExceptionTools;

mixin(MakeException("DataTransformException", "An error occurred while transforming this data.", "Exception"));


/// An object used to manipulate on a DataSource by performing a transform operation on data passing through it.
/// Multiple transformers may operate on the same set of data, so a (constant) order property is enforced
/// to indicate which transforms should have priority over one another. OutputSources are applied in lowest to 
/// highest order, whereas InputSources are applied highest to lowest order to undo the results of the OutputSource transforms.
abstract class DataTransform  {

	/// Creates a new DataTransform with the given fixed order.
	this(int order) {
		this._order = order;
	}

	// TODO: Need to be able to add a transform to an already started IOAction.
	// So, two options.
	// The first is a rather ugly one with suspendTransforms and resumeTransforms.
	// It is simple though and doesn't result in any conflicts with order.
	// Once transforms are resumed it would just apply all transforms to all buffered data.
	// The second is to allow adding arbitrary transforms at random points during the operation.
	// This is nicer, but the user has to make sure the order is correct manually.
	// Will probably still go with second though, it's more natural and nicer.

	// Except the second option doesn't work because you're going to do the transform in the middle
	// of the read (ex: read first 8 bytes of header indicating uncompressed size and such, then
	// add an inflate transform). So it wouldn't apply to the data you're in the process of reading
	// as that's not buffered, and modifying it would be a poor choice.
	// But you can't just tell the transform to manually transform it, as it automatically transforms
	// all data when it's added, so now ordering is not preserved.
	// Of course, could manually transform before adding the transform to the DataSource, which
	// would actually work.
	// Really, there's no good solution for these problems I can think of, so then just do #2.]
	// Also #1 doesn't work well since you may want to switch protocols later.
	// One issue though is that you can't really remove transforms easily.
	// Could technically apply the opposite transform to remove...

	// Instead of either of the above, go with option 2 however with each transform being able to
	// transform for both reads and writes. This means that when a new transform is added in the middle
	// of the transform collection, the existing transforms could be applied to untransform the buffer
	// then another transform made to retransform it in proper order.
	// ...except that doesn't work because most transforms operate on streams.
	// For example you can't undo a Deflate because it requires the previous data to be kept.

	/// Transforms the given buffer, returning the result of the transformation.
	/// The result may or may not be the same instance of Buffer that was passed in.
	abstract ubyte[] transform(ubyte[] buffer);

	/// Indicates the order that this transform should be applied relative to other transforms.
	/// When transforming an OutputSource, transforms are applied in lowest to highest order.
	/// When transforming an InputSource, transforms are applied in highest to lowest order.
	/// This value must remain constant once the transform is added.
	@property int order() const @safe pure nothrow {
		return _order;
	}

private:
	int _order;
}

/// Allows for implementation of the IDataTransform interface to operate on InputSources.
abstract class InputTransform : DataTransform {
	/// Creates a new InputTransform with the given order.
	this(int order) {
		super(order);
	}
}

/// Allows for implementation of the IDataTransform interface to operate on OutputSources.
abstract class OutputTransform : DataTransform {
	/// Creates a new OutputTransform with the given order.
	this(int order) {
		super(order);
	}

	/// Called after the last write of an OutputSource to perform any requirement for
	/// writing footers or general flushing operations.
	/// The returned value will be written in the same order OutputTransforms are applied.
	/// The default implementation returns an empty array.
	ubyte[] flush() { return null; }
}

/// Indicates how to perform a DataTransform transform operation.
enum DataTransformOperation {
	/// The transform should be done as a read operation, that is highest to lowest order.
	read = 0,
	/// The transform should be done as a write operation, that is lowest to highest order.
	write = 1
}

/// Represents an ordered collection of DataTransforms.
/// The only valid operations are adding a transform, and applying all transforms on a slice of data.
/// Removing a transform once it's added is not possible because it would require an opposite transform
/// to be applied on all data, which is in many cases not possible to efficiently implement.
class DataTransformCollection(T : DataTransform) {

	this(DataSource parent) {
		this._parent = parent;
		this._mutex = new Mutex();
	}

	/// Adds the given DataTransform to this collection.
	/// See $(D DataTransform) for details about transformations and priority.
	/+/// Adding an InputTransform after an IOAction has started is allowed, however there
	/// may already be data being processed by the OutputSource that may not receive that transform.
	/// For example if you are adding a transform during a StreamOutput read, you would need to 
	/// transform the rest of that read's output yourself.
	/// In addition, all buffered data will be transformed immediately, so if there are order
	/// dependent transforms that must be applied before or after this transform, they will
	/// not be applied in proper order and data corruption may occur.
	/// As such, it is recommended to avoid adding InputTransforms to an already started
	/// IOAction, but if you must do so, do so with care.+/
	/// Bugs:
	/// 	Adding a transform after the action has already started is not yet allowed.
	void add(T transform) {
		synchronized(_parent) {
			bool _inserted = false;
			foreach(i, existing; _transforms) {
				if(existing.order > transform.order) {
					_transforms.insertInPlace(i, transform);
					_inserted = true;
					break;
				}
			}
			if(!_inserted)
				_transforms ~= transform;
			static if(is(T : InputTransform)) {
				InputSource input = cast(InputSource)_parent;
				if(input && input.Action && input.Action.HasBegun)
					input.Action.NotifyInputTransformAdded(transform);
			}
		}
	}

	/// Applies all transforms in this collection to the given data.
	/// The $(D operation) parameter indicates whether to perform the transform in
	/// highest to lowest order (read) or lowest to highest (write).
	ubyte[] transform(ubyte[] data, DataTransformOperation operation) {
		ubyte[] result = data;
		synchronized(_parent) {
			if(operation == DataTransformOperation.read) {
				foreach(transform; _transforms.retro)
					result = transform.transform(result);
			} else if(operation == DataTransformOperation.write) {
				foreach(transform; _transforms)
					result = transform.transform(result);
			} else {
				assert(0);
			}
		}
		return result;
	}

	static if(is(T : OutputTransform)) {
		/// Invokes flush on all OutputSources in the same order a transform would be performed.
		/// Returns the concatenated output in the above order.
		ubyte[] flush() {
			ubyte[] result;
			synchronized(_parent) {
				foreach(transform; _transforms)
					result ~= transform.flush();
			}
			return result;
		}
	}

	// TODO: An addManagedTransform which uses malloc to create the class then frees upon IOAction completion.

private:
	T[] _transforms;
	DataSource _parent;
	Mutex _mutex;
}