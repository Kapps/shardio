﻿module ShardIO.IOManager;
private import std.algorithm;
private import std.stdio;
private import std.exception;
private import ShardIO.IOAction;
private import std.parallelism;
import ShardTools.Untyped;
import ShardTools.AsyncAction;

/// Manages any IOActions.
class IOManager  {

public:
	
	/// Initializes a new instance of the IOManager object.
	/// Params:
	///		NumWorkers = The number of threads to process data when available. A value of zero sets a safe value related to the number of cpus / threads.
	this(size_t NumWorkers = 0) {			
		if(NumWorkers == 0)
			Pool = new TaskPool(max(defaultPoolThreads / 1, 1));
		else
			Pool = new TaskPool(NumWorkers);		

		// Start off as a daemon so we don't block the program exiting.
		Pool.isDaemon = true; 
	}	

	/// Gets a default instance of the IOManager, lazily initialized with the same number of threads as the default TaskPool implementation.
	/// It is generally recommended to use this instance as opposed to creating your own. But there may be situations where more fine-grained control is desired,
	/// such as to prevent long operations from blocking smaller ones if there are too many of them.
	@property static IOManager Default() {
		synchronized {
			if(_Default is null)
				_Default = new IOManager();
			return _Default;
		}
	}

	/// Gets the number of worker threads this instance uses.
	@property size_t NumWorkers() const {
		return Pool.size;
	}

	package void NotifyActionStart(IOAction Action) {
		// Every time we queue an action, we must make sure that the pool is not set to daemon state.
		// When it's set to be a daemon, the program will exit before all tasks are finished.
		// This means our IO could stop before it's complete and result in corruption.
		// Note that this method is different from QueueAction as that may get called multiple times.
		synchronized(this) {
			_ActiveActions++;
			if(_ActiveActions == 1)
				Pool.isDaemon = false;
		}
		Action.NotifyOnComplete(Untyped.init, &OnActionComplete);
	}

	/// Queues the given action to be executed.
	/// Params:
	/// 	Action = The action to queue.					
	package void QueueAction(IOAction Action) {		
		Pool.put(task(&Action.ProcessData));
	}

	private void OnActionComplete(Untyped result, AsyncAction action, CompletionType type) {
		// We have to set the pool to use daemon threads when we have no actions queued.
		// Otherwise the program will never exit.
		synchronized(this) {
			_ActiveActions--;
			if(_ActiveActions == 0)
				Pool.isDaemon = true;
		}
	}
	
private:
	TaskPool Pool;
	size_t _ActiveActions = 0;
	static __gshared IOManager _Default;
}