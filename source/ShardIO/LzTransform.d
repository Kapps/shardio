﻿/// Provides DataTransforms for working with the LZ4 compression algorithm.
/// This module is versioned out unless Have_lz4 is defined (which is automatic with dub including it).
/// When Have_lz4 is defined, the lz4 library will need to be linked.
/// $(RED This module is not currently stable, and the compression algorithm may change in the future to support dependent blocks.)
module ShardIO.LzTransform;

version(none) {
version(Have_lz4) {
import deimos.lz4.lz4;
import ShardTools.Buffer;
import ShardTools.BufferPool;

public import ShardIO.DataTransform;
import std.exception;

// TODO: This module is currently inefficient, which should be changed.
// First, it performs needless large allocations without releasing old buffers.
//	This is somewhat fixed by releasing transform buffers, as we technically own them.
//	Still allocates, but does so with smaller blocks.
//	Also, buffers should probably be structs and this would be fixed...
// Second, it does not use dependent blocks for compression, which is relatively easy to fix.
// (aka, LZ4_compress_continue instead of LZ4_compress).
// Using the above may also fix allocations by using a single buffer.

/// Utilizes the LZ4 algorithm to compress output data.
class LzCompressTransform : OutputTransform {

	this(int order) {
		super(order);
		/+buffer = BufferPool.Global.Acquire(192 * 1024);
		lz = LZ4_create(cast(char*)buffer.FullData.ptr);
		enforce(lz);+/
	}

	override ubyte[] transform(ubyte[] buffer) {
		int length = LZ4_compressBound(buffer.length);
		// Technically no need to use limited output, but just to be safe.
		Buffer buff = BufferPool.Global.Acquire(length);
		int written = LZ4_compress_limitedOutput(cast(char*)buffer.ptr, cast(char*)buff.FullData.ptr, cast(int)buffer.length, cast(int)buff.Capacity);
		if(written == 0)
			throw new DataTransformException("An error occurred while compressing the data.");
		buff.AdvancePosition(written);
		// We own the old buffer, let it go back into the pool.
		BufferPool.Global.Release(Buffer.FromExistingData(buffer));
		return buff.Data;
	}

	/+~this() {
		LZ4_free(lz);
	}+/

private:
	/+void* lz;
	Buffer buffer;+/
}

/// Utilizes the LZ4 algorithm to decompress input data.
class LzUncompressTransform : InputTransform {

	this(int order) {
		super(order);
	}

	override ubyte[] transform(ubyte[] buffer) {
		// TODO: How can we get the uncompressed size here?
	}

}
}
}