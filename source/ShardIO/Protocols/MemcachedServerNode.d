module ShardIO.Protocols.MemcachedServerNode;
/+public import ShardIO.DistributedServerNode;

/// Provides information about a single memcached server that can receive requests from the client.
class MemcachedServerNode : IDistributedServerNode {
	this() {
		// Constructor code
	}

private:

}
+/