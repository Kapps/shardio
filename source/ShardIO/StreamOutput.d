module ShardIO.StreamOutput;
import ShardTools.AsyncAction;
public import ShardTools.StreamReader;
import ShardIO.IOAction;
import ShardIO.OutputSource;
import std.typecons;
public import ShardTools.Untyped;

/// An alias to a delegate for the callback that is invoked upon each read.
/// The $(D state) parameter is a user-defined state variable that is ignored by the $(StreamOutput).
/// The $(D reader) parameter is a stack-allocated scoped StreamReader for performing this read.
/// The data that is consumed by the callback may be stored and modified at will.
/// The remaining data (that is anything past the position of the reader at the end of the call)
/// is still owned by the StreamOutput and may not be stored nor modified.
alias DataRequestFlags delegate(Untyped state, scope StreamReader reader) StreamOutputCallback;

/// Provides an OutputSource that buffers input allowing it to be read as a stream.
/// Data is read by invoking a callback upon each chunk received, then using a StreamReader to read that chunk. 
/// The unread bytes are then buffered for the next call.
/// The reader callback returns the same DataRequestFlags that an OutputSource would.
class StreamOutput : OutputSource {

	/// Creates a new StreamOutput with the given callback and state.
	/// The state is passed in to the callback on each call, and is not used by StreamOutput directly.
	this(Untyped state, StreamOutputCallback callback) {
		this.callback = callback;
		this.state = state;
	}

	/// Notifies the callback to read the next chunk of data.
	override DataRequestFlags ProcessNextChunk(ubyte[] Chunk, out size_t BytesHandled) {
		//StreamReader reader = scoped!StreamReader(Chunk, false);
		// TODO: Using the above causes a segfault. Bug?
		auto reader = new StreamReader(Chunk, false);
		auto flags = callback(state, reader);
		BytesHandled = reader.Position;
		return flags;
	}

	/// Informs the StreamOutput after a callback returns Waiting that it is ready for more data.
	void notifyDataReady() {
		super.NotifyReady();
	}

	/// The StreamOutput does nothing when PerformClose is called.
	protected override void PerformClose() {
		// no-op
	}

private:
	StreamOutputCallback callback;
	Untyped state;
}

