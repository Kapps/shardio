﻿module ShardIO.ObjectOutput;
public import ShardTools.Event;
public import ShardIO.OutputSource;
import ShardTools.Buffer;

private struct ReadOperation {
	void function() reader;
	size_t byteCount;
	string stateName;
}

private template isDirectlyReadable(T) {
	// Built in type, POD struct. Not array / AA, those handled separately.
}

/// Provides a class to contain a collection of read operations performed
/// in order to fully read an object asynchronously from an InputSource.
class ReadSequence {

	void appendRead(T)() {

	}
}

/// Returns a ReadSequence for the given type, using the sequence specified by
/// the type, or else a default one if none was specified.
ReadSequence readSequence(T)() {
	// Check array, return int and then many.
	// Check 
}


/// Provides an OutputSource that uses a ReadSequence in order to perform
/// asynchronous reads on an InputSource and generate an object of a 
/// desired type from the result.
/// In many cases, a default ReadSequence can be generated using
/// the UFCS call $(D readSequence). If this is insufficient,
/// one may define their own $(D readSequence) on the type which will be used
/// instead of the automatically generated one.
class ObjectOutput {
	// TODO: Automatically generated one should maybe use std.serialization somehow.

}


// This design is lousy, make a new one.
/+
/+/// Provides an ObjectOutput to parse an object written by MessagePack.
public T ParseMessagePackedObject(T)(ubyte[] data) {
	// return NotEnoughBytes to make next Data have more bytes avail.
	// Provide a max number of bytes stored, and if too many, abort the parse.
	return T.init;
}+/

/// Represents an OutputSource that parses input data to create an object, then operates on the completed objects.
/// Params:
///		T = The type of the object to create.
abstract class ObjectOutput(T) : OutputSource {	

public:

	alias Event!(void, T) ObjectCreatedEvent;

	/// Initializes a new instance of the ObjectOutput object.
	this() {
		_ObjectCreated = new ObjectCreatedEvent();
	}

	/// Gets an event called whenever an object is finished being created.
	@property ObjectCreatedEvent ObjectCreated() {
		return _ObjectCreated;
	}

protected:	

	/// Notifies the ObjectOutut that an instance has been fully created.
	/// Params:
	/// 	Obj = The object that was created.
	void NotifyObjectBuild(T Obj) {
		ObjectCreated.Execute(Obj);
	}

private:
	ObjectCreatedEvent _ObjectCreated;
}+/