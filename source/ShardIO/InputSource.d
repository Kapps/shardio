﻿module ShardIO.InputSource;

import std.array;

public import ShardIO.DataSource;
public import ShardIO.DataTransform;

/// Provides information about data read from an InputSource.
enum DataFlags {
	/// No flags are set.
	None = 0,
	/// Indicates the data may be safely stored without needing to be copied.
	/// Setting this flag removes ownership from the caller, and allows the data to be used directly (and possibly changed).
	AllowStorage = 1
}

/// Provides a data source used as an input for an operation.
abstract class InputSource : DataSource {

public:
	/// Initializes a new instance of the InputSource object.
	this() {
		this._transforms = new DataTransformCollection!InputTransform(this);
	}

	/// Returns the transforms that apply to this InputSource.
	DataTransformCollection!InputTransform transforms() {
		return _transforms;
	}

package:
	DataRequestFlags InvokeGetNextChunk(size_t RequestedSize, scope void delegate(ubyte[], DataFlags) Callback) {
		return GetNextChunk(RequestedSize, Callback);
	}

protected:

	/// Called by the IOAction after this InputSource notifies it is ready to have input received.
	/// The InputSource should have roughly RequestedSize bytes ready and then invoke Callback with the available data.
	/// If the InputSource is unable to get an acceptable number of bytes without blocking, then Waiting should be returned.
	/// The RequestedSize parameter is only a hint; as much or little data may be passed in as desired. The unused data will then be buffered.
	/// See $(D, DataRequestFlags) and $(D, DataFlags) for more information as to what the allowed flags are.
	/// Params:
	/// 	RequestedSize = A rough number of bytes requested to be passed into Callback. This is simply to prevent buffering too much, so if the data is already in memory, just pass it in.
	/// 	Callback = The callback to invoke with the data.
	/// Ownership:
	///		Any Data passed in will have ownership transferred away from the caller if DataFlags includes the AllowStorage bit.
	abstract DataRequestFlags GetNextChunk(size_t RequestedSize, scope void delegate(ubyte[], DataFlags) Callback);

	/// Notifies the IOAction owning this InputSource that there is some data ready to be written.
	/// Take care to avoid deadlocks when calling this method, as this will cause the 
	/// IOAction to be locked and may be called while another thread is running GetNextChunk.
	final void NotifyDataReady() {		
		if(Action) // If not, we're ready prior to a call to Start, so it's fine anyways.
			Action.NotifyInputReady();	
	}

private:
	DataTransformCollection!InputTransform _transforms;
}